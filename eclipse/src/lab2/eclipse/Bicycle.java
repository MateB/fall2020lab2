//Mate Barabas 1834578
package lab2.eclipse;
public class Bicycle
{
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
		public Bicycle(String manufacturer, int numberGears, double maxSpeed)
		{
			this.manufacturer = manufacturer;
			this.numberGears = numberGears;
			this.maxSpeed = maxSpeed; 
		}
		
		public String getManu()
		{
			return this.manufacturer;
		}
		
		public int getNumberGears()
		{
			return this.numberGears;
		}
		
		public double getMaxSpeed()
		{
			return this.maxSpeed;
		}
		
		public String toString()
		{
			return "Manufacturer: " + this.manufacturer + "\n numberGears: " + this.numberGears + "\n maxSpeed: " + this.maxSpeed;
		}
}

//Mate Barabas 1834578
package lab2.eclipse;
import lab2.eclipse.*;

public class BikeStore 
{
	public static void main(String[] args) 
	{
		Bicycle[] bike = new Bicycle[4];
		
		bike[0] = new Bicycle("speedBikes", 37, 600.0);
		bike[1] = new Bicycle("Dan's Bikes", 5, 50.0);
		bike[2] = new Bicycle("CanadasBikes", 20, 400.0);
		bike[3] = new Bicycle("bikesTheBike", 13, 76.0);
		
		for (int i=0; i<bike.length; i++)
		{
			System.out.println(bike[i].toString());
		}
	}

}
